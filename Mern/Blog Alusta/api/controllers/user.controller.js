import bcryptjs from 'bcryptjs'
import { errorHandler } from '../utils/error.js'
import User from '../models/user.model.js'

export const test = (req, res) => {
    res.json({message: 'API toimii!'})
}

export const updateUser = async (req, res, next) => {
    if (req.user.id !== req.params.userId) {
        return next(errorHandler(403, 'Sinulla ei ole oikeutta päivittää tämän käyttäjän tietoja'))
    }
    if (req.body.password) {
        if(req.body.password.length < 6) {
            return next(errorHandler(400, 'Salasana pitää olla vähintään 6 merkkiä pitkä'))
        }
        req.body.password = bcryptjs.hashSync(req.body.password, 10)
    }
    if (req.body.username) {
        if(req.body.username.length < 7 || req.body.username.length > 20) {
            return next(errorHandler(400, 'Käyttäjän nimi pitää olla vähintään 7 ja enintään 20 merkkiä pitkä'))
        }
        if (req.body.username.includes(' ')) {
            return next(errorHandler(400, 'Käyttäjä nimessä ei saa olla välilyötiä'))
        }
        if (req.body.username !== req.body.username.toLowerCase()) {
            return next(errorHandler(400, 'Käyttäjä nimen pitää olla pienillä kirjaimilla'))
        }
        if (!req.body.username.match(/^[a-zA-Z0-9]+$/)) {
            return next(errorHandler(400, 'Käyttäjä nimessä saa olla pelkästään kirjaimia ja numeroita'))
        }
    }
    try {
        const updateUser = await User.findByIdAndUpdate(req.params.userId, {
            $set: {
                username: req.body.username,
                email: req.body.email,
                profilePicture: req.body.profilePicture,
                password: req.body.password,
            },
        }, {new: true})
        const { password, ...rest } = updateUser._doc
        res.status(200).json(rest)
    } catch (error) {
        next(error)
    }
}

export const deleteUser = async (req, res, next) => {
    if (!req.user.isAdmin && req.user.id !== req.params.userId) {
        return next(errorHandler(403, 'Sinulla ei ole oikeutta poistaa tätä käyttäjää'))
    }
    try {
        await User.findByIdAndDelete(req.params.userId)
        res.status(200).json('Käyttäjä poistettiin onnistuneesti')
    } catch (error) {
        next(error)
    }
}

export const signout = (req, res, next) => {
    try {
        res.clearCookie('access_token').status(200).json('Käyttäjä on kirjautunut ulos')
    } catch (error) {
        next(error)
    }
}

export const getUsers = async (req, res, next) => {
    if (!req.user.isAdmin) {
        return next(errorHandler(403, 'Sinulla ei ole oikeuksia nähdä kaikkia käyttäjiä'))
    }
    try {
        const startIndex = parseInt(req.query.startIndex) || 0
        const limit = parseInt(req.query.limit) || 9
        const sorrDirection = req.query.sort === 'asc' ? 1 : -1

        const users = await User.find()
            .sort({ createdAt: sorrDirection})
            .skip(startIndex)
            .limit(limit)
        
        const userWithoutPassword = users.map((user) => {
            const { password, ...rest } = user._doc
            return rest
        })

        const totalUsers = await User.countDocuments()

        const now = new Date()

        const oneMountAgo = new Date(
            now.getFullYear(),
            now.getMonth() - 1,
            now.getDate()
        )

        const lastMountUsers = await User.countDocuments({
            createdAt: { $gte: oneMountAgo }
        })

        res.status(200).json({
            users: userWithoutPassword,
            totalUsers,
            lastMountUsers,
        })
        
    } catch (error) {
        next(error)
    }
}

export const getUser = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.userId)
        if (!user) {
            return next(errorHandler(404, 'Käyttäjiä ei löytynyt'))
        }
        const { password, ...rest } = user._doc
        res.status(200).json(rest)
    } catch (error) {
        next(error)
    }
}