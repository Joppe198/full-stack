import mongoose from 'mongoose'

/*userSchemalla tehdään user tietokanta pohja jossa näkee mitä kaikea tietoa user käyttäjästä
tarvitaan: käyttäjänimi, sähköposti ja salasana, käyttäjänimi ja sähköposti ovat unikkeja, 
eli sovelluksessa voidaan käyttää pelkästää yhtä saman nimistäkäyttäjää ja samaa sähköpostiosoitetta  */

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password:{
        type: String,
        required: true,
    },
    profilePicture:{
        type:String,
        default: 'https://firebasestorage.googleapis.com/v0/b/mern-blog-17f5e.appspot.com/o/tausta%2FMyEmoji_20240612_204831_0.png?alt=media&token=b15cd1f8-6fe2-481d-abee-8dd25076c79a',
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
}, {timestamps: true}
)
const User = mongoose.model('User', userSchema)

export default User