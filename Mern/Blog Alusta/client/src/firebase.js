// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: import.meta.env.VITE_FIREBASE_API_KEY,
  authDomain: "mern-blog-17f5e.firebaseapp.com",
  projectId: "mern-blog-17f5e",
  storageBucket: "mern-blog-17f5e.appspot.com",
  messagingSenderId: "667453792946",
  appId: "1:667453792946:web:163ad5c85bbc7525169fab"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);