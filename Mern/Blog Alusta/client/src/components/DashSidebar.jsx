import { useEffect, useState } from 'react'
import { Sidebar } from 'flowbite-react'
import { HiAnnotation, HiArrowSmRight, HiChartPie, HiDocumentText, HiOutlineUserGroup, HiUser } from 'react-icons/hi'
import { Link, useLocation } from 'react-router-dom'
import { signoutSuccess } from '../redux/user/userSlice'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'

export default function DashSidebar() {
    const location = useLocation()
    const dispatch = useDispatch()
    const {currentUser} = useSelector(state => state.user)
    const [tab, setTab] = useState('')
  
    useEffect(() => {
        const urlParams = new URLSearchParams(location.search)
        const tabFromUrl = urlParams.get('tab')
        if (tabFromUrl) {
            setTab(tabFromUrl)
        }
    }, [location.search])

    const handleSignout = async () => {
        try {
            const apiUrl = import.meta.env.VITE_API_URL;
            const res = await fetch(`${apiUrl}/api/user/signout`, {
                method: 'POST',
            })
        const data = await res.json()
        if (!res.ok) {
            console.log(data.message)
        } else {
            dispatch(signoutSuccess())
        }
        } catch (error) {
            console.log(error.message)
        }
    }
return (
    <Sidebar className='w-full md:w-56'>
        <Sidebar.Items>
            <Sidebar.ItemGroup className='flex flex-col gap-1'>
                {
                    currentUser && currentUser.isAdmin && (
                        <Link to='/dashboard?tab=dash'>
                            <Sidebar.Item 
                                active={tab === 'dash' || !tab}
                                icon={HiChartPie} 
                                labelColor='dark'
                                as='div'
                            >
                                Dashboard
                            </Sidebar.Item>
                        </Link>
                    )
                }
                <Link to='/dashboard?tab=profile'>
                    <Sidebar.Item 
                        active={tab === 'profile'} 
                        icon={HiUser} 
                        label={currentUser.isAdmin ? 'Admin' : 'User'} 
                        labelColor='dark'
                        as='div'
                    >
                        Profiili
                    </Sidebar.Item>
                </Link>
                {currentUser.isAdmin && (
                <Link to='/dashboard?tab=posts'>
                    <Sidebar.Item
                        active={tab === 'posts'}
                        icon={HiDocumentText}
                        as='div'
                    >
                        Viestit
                    </Sidebar.Item>
                </Link>
                )}
                {currentUser.isAdmin && (
                <>
                <Link to='/dashboard?tab=users'>
                    <Sidebar.Item
                        active={tab === 'users'}
                        icon={HiOutlineUserGroup}
                        as='div'
                    >
                        Käyttäjä
                    </Sidebar.Item>
                </Link>
                <Link to='/dashboard?tab=comments'>
                    <Sidebar.Item
                        active={tab === 'comments'}
                        icon={HiAnnotation}
                        as='div'
                    >
                        Kommnetit
                    </Sidebar.Item>
                </Link>
                </>
                )}
                <Sidebar.Item onClick={handleSignout} icon={HiArrowSmRight} className='cursor-pointer'>
                    Kirjaudu Ulos
                </Sidebar.Item>
            </Sidebar.ItemGroup>
        </Sidebar.Items>
    </Sidebar>
    )
}
